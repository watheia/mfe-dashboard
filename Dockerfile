# Check out https://hub.docker.com/_/node to select a new base image
FROM node:14-alpine as buildEnv

RUN apk upgrade && apk add python make g++

# Set to a non-root built-in user `node`
USER node

# Create app directory (with user `node`)
RUN mkdir -p /home/node/app

WORKDIR /home/node/app

# Build app
COPY --chown=node . .
RUN yarn build

FROM nginx:alpine

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=8080

COPY --from=buildEnv /home/node/app/dist /usr/share/nginx/html
